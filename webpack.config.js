var path = require('path');
var webpack = require('webpack');
const autoprefixer = require('autoprefixer');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: './src/main.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.bundle.js'
    },
    devtool: "source-map",//Sourcemap support to can debbug source code
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    module: {
        rules: [
            { test: /\.handlebars$/, loader: "handlebars-loader" },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        attrs: [':data-src']
                    }
                },
                exclude: path.resolve(__dirname, 'src/index.html')//en el caso de tener un archivo base sin usar
                //HtmlWebpackPlugin
            },
            {
                test: /\.(scss|css)$/,
                use: ['style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            autoprefixer: {
                                browsers: ["last 2 versions"]
                            },
                            sourceMap: true,
                            plugins: () => [
                                autoprefixer
                            ]
                        },
                    }
                ]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                enabled: true,
                            },
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                            webp: {
                                quality: 75
                            }
                        }
                    },
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'static/',
                            useRelativePath: true,
                        }
                    },
                ]
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
            title: 'Webpack 4',
            chunks: ['app']
        }),
        //A continuación podemos importar más htmls con sus
        //respectivos bundles si sabemos cuántos son, de lo contrario
        //usamos la alternativa de cargadores de html Línea: 
        /*new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/app/index/index.html',
            chunks: ['otroEntryPoint']
        }),*/
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            //'process.env.NODE_ENV': JSON.stringify('production')
            'process.env.NODE_ENV': JSON.stringify('dev')
        })],
    devServer: {
        contentBase: path.join(__dirname, "/dist/"),
        compress: true,
        inline: true,
        port: 8888,
        open: true,
        publicPath: "/dist/",
        hot: true,
        watchOptions: {
            poll: true
        }
    },
    stats: { colors: true }
};